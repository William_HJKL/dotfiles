domain = IMAP {
  server = 'mail.domain.com',
  username = 'william@domain.com',
}

options.timeout = 120
options.subscribe = true
options.create = true

msgs2001 = icilenet['INBOX']:sent_before('31-Dec-2001')
msgs2001:move_messages(icilenet['INBOX/archives-2001'])

msgs2002 = icilenet['INBOX']:sent_before('31-Dec-2002')
msgs2002:move_messages(icilenet['INBOX/archives-2002'])

