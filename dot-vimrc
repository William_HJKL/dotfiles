" =============================================================
" Title :		dot-vimrc
" Author :		William HOFFMANN - william@redhat.com
" Last Modified :	2014-02-28-17:12:47 CET
" =============================================================
set t_Co=256
filetype plugin on
set title
set nocompatible
set backspace=indent,eol,start
set syntax=on
set autoindent
set smartindent
set ruler
set ts=4
set textwidth=72
set fileencoding=utf-8
set encoding=utf-8
set nopaste
set splitbelow
set modeline        " last lines in document sets Vim mode
set modelines=3     " number lines checked for mode lines
"=============================================================
set spell
set spelllang=en,fr
"=============================================================
colorscheme  evening
set t_Co=256
set backup
autocmd BufWritePre *.conf exe "set backupext=.".strftime("%Y%m%d%H%M%S")
autocmd BufWritePre *rc exe "set backupext=.".strftime("%Y%m%d%H%M%S")
"
" Template.
"
"if has("autocmd")
"	au BufNewFile *.txt 0r ~/.vim/templates/squel.txt
"	au BufNewFile *.sh 0r ~/.vim/templates/squel.sh
"endif

syn keyword FIXME TODO IDEA

" Date/Time/Sig...
iab xstamp <C-R>=strftime("%Y-%m-%d %R")<CR>
iab rpmcl <C-R>='* '. strftime("%a %b %d %Y"). ' - William HOFFMANN <william@icilenet.com>-'<CR>
iab lastmod <C-R>="\# Last Modified :	" . strftime("%Y-%m-%d-%T %Z")<CR>
iab _mod <C-R>="\# Modif\. :	WH- william@icilenet.com	". strftime("%Y-%m-%d-%R").""<CR>
"=============================================================
" Redundant Whitespace ...
"
highlight RedundantWhitespace ctermbg=red guibg=red
match RedundantWhitespace /\s\+$\| \+\ze\t/

let mapleader = ","
" Underline ( asciidoc )
nmap ,= yyp^v$r=o<Esc>
nmap ,1 yyp^v$r-o<Esc>
nmap ,2 yyp^v$r~o<Esc>
nmap ,3 yyp^v$r^o<Esc>
nmap ,4 yyp^v$r+o<Esc>
nmap ,_ yyp^v$r_o<Esc>
nmap ,* yyp^v$r*o<Esc>
" Remove whitespace at the beginning of the line.
nmap _S :%s/^\s\+//<CR>
" Remove whitespace at the end of the line.
nmap _R :%s/\s\+$//<CR>


"
" Transparent editing of gpg encrypted files.
" Placed Public Domain by Wouter Hanegraaff <wouter@blub.net>
" (asc support and sh -c"..." added by Osamu Aoki)
augroup aencrypted
        au!

        " First make sure nothing is written to ~/.viminfo while editing
        " an encrypted file.
        autocmd BufReadPre,FileReadPre      *.asc set viminfo=
        " We don't want a swap file, as it writes unencrypted data to disk
        autocmd BufReadPre,FileReadPre      *.asc set noswapfile
        " Switch to binary mode to read the encrypted file
        autocmd BufReadPre,FileReadPre      *.asc set bin
        autocmd BufReadPre,FileReadPre      *.asc let ch_save = &ch|set ch=2
        autocmd BufReadPost,FileReadPost    *.asc '[,']!sh -c "gpg --decrypt 2> /dev/null"
        " Switch to normal mode for editing
        autocmd BufReadPost,FileReadPost    *.asc set nobin
        autocmd BufReadPost,FileReadPost    *.asc let &ch = ch_save|unlet ch_save
        autocmd BufReadPost,FileReadPost    *.asc execute ":doautocmd BufReadPost " . expand("%:r")

        " Convert all text to encrypted text before writing
        autocmd BufWritePre,FileWritePre    *.asc   '[,']!sh -c "gpg --default-recipient-self -ae 2>/dev/null"
        " Undo the encryption so we are back in the normal text, directly
        " after the file has been written.
        autocmd BufWritePost,FileWritePost    *.asc   u
augroup END
augroup bencrypted
        au!

        " First make sure nothing is written to ~/.viminfo while editing
        " an encrypted file.
        autocmd BufReadPre,FileReadPre      *.gpg set viminfo=
        " We don't want a swap file, as it writes unencrypted data to disk
        autocmd BufReadPre,FileReadPre      *.gpg set noswapfile
        " Switch to binary mode to read the encrypted file
        autocmd BufReadPre,FileReadPre      *.gpg set bin
        autocmd BufReadPre,FileReadPre      *.gpg let ch_save = &ch|set ch=2 
        autocmd BufReadPost,FileReadPost    *.gpg '[,']!sh -c "gpg --decrypt 2> /dev/null"
        " Switch to normal mode for editing 
        autocmd BufReadPost,FileReadPost    *.gpg set nobin 
        autocmd BufReadPost,FileReadPost    *.gpg let &ch = ch_save|unlet ch_save
        autocmd BufReadPost,FileReadPost    *.gpg execute ":doautocmd BufReadPost " . expand("%:r")

        " Convert all text to encrypted text before writing
        autocmd BufWritePre,FileWritePre    *.gpg   '[,']!sh -c "gpg --default-recipient-self -e 2$gt;/dev/null"
        " Undo the encryption so we are back in the normal text, directly
        " after the file has been written.
        autocmd BufWritePost,FileWritePost    *.gpg   u
augroup END

"" Settings for :TOhtml
let html_number_lines=1
let html_use_css=1
let use_xhtml=1

" vim: tw=72 ts=4:
highlight CursorLine                    cterm=none ctermbg=DarkBlue
set cursorline

autocmd BufNewFile,BufRead *.txt         set ft=asciidoc

set tabstop=8
set softtabstop=2
set shiftwidth=2
set expandtab
set cinoptions=>4,n-2,{2,^-2,:0,=2,g0,h2,t0,+2,(0,u0,w1,m1


